var gulp = require('gulp');
var sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    concatCss = require('gulp-concat-css'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();
    
//browserSync
gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: "./dist/"
    }
  
  });
});
// HTML
gulp.task('markup', function () {
	return gulp.src('./src/*.html')
		.pipe(gulp.dest('./dist/'));
});
//sass
gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.reload({stream: true}));
});
// ConcatCss
gulp.task('concatCss', function() {
	return gulp.src('./src/css/*.css')
	    .pipe(concatCss('style.css'))
	    .pipe(gulp.dest('./dist/css/'))
	    .pipe(browserSync.reload({stream: true}));
});
//img 
gulp.task('imageReplace', function () {
	return gulp.src('./src/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./dist/img/'));
});
//Watcher
gulp.task('watch', ['browser-sync', 'sass', 'markup', 'imageReplace'], function() {
    gulp.watch('src/sass/**/*.scss', ['sass']);
    // gulp.watch('src/css/*.css', ['concatCss']);
    gulp.watch('src/*.html', ['markup']);
    gulp.watch('src/img/**/*.png', ['imageReplace']);
    gulp.watch('dist/*.html').on('change', browserSync.reload);
});
